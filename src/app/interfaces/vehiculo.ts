export interface Vehiculo {
    marca: String;
    tipo: String;
    anio: String;
}