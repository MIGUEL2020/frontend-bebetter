import { Vehiculo } from "./vehiculo";

export interface persona {
  cc: String;
  direccion: String;
  fecha_nacimiento: Date;
  municipio: string;
  nombres: string;
  profesion: string;
  sexo: string;
  telefono: String;
  vehiculo: Vehiculo

}