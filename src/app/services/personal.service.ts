import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { persona } from '../interfaces/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  constructor(private httpClient: HttpClient) {
  }

  getPersonal(): Observable<persona[]> {
    return this.httpClient.get<persona[]>('https://personal-profesional-default-rtdb.firebaseio.com/personal-profesional.json');
  }

  addPersona(personas: persona[]): Observable<persona[]> {
    return this.httpClient.put<persona[]>('https://personal-profesional-default-rtdb.firebaseio.com/personal-profesional.json', personas);
  }

  deletePerson(pos: number): Observable<any> {
    return this.httpClient.delete<persona>('https://personal-profesional-default-rtdb.firebaseio.com/personal-profesional/' + pos + '.json');
  }

  editPerson(pos: number, persona: persona): Observable<any> {
    return this.httpClient.put<persona>('https://personal-profesional-default-rtdb.firebaseio.com/personal-profesional/' + pos + '.json', persona);
  }
}
