import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { persona } from '../../../interfaces/persona';

@Component({
  selector: 'app-form-persona',
  templateUrl: './form-persona.component.html',
  styleUrls: ['./form-persona.component.css']
})
export class FormPersonaComponent implements OnInit {

  @Output() btnGuardarEvent = new EventEmitter();
  @Output() btnEditEvent = new EventEmitter();

  @Input() fromCrear: boolean = false;
  @Input() persona: persona = {
    cc: '',
    direccion: '',
    fecha_nacimiento: new Date(),
    municipio: '',
    nombres: '',
    profesion: '',
    sexo: '',
    telefono: '',
    vehiculo: {
      marca: '',
      tipo: '',
      anio: ''
    }
  }
    ;

    generos : string[] = ["Femenino","Masculino"]

  formPersona: FormGroup;
  formVehiculo: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    console.log(this.persona);

    this.formPersona = this.formBuilder.group({
      cc: '',
      direccion: '',
      fecha_nacimiento: new Date(),
      municipio: '',
      nombres: '',
      profesion: '',
      sexo: '',
      telefono: ''
    })
    this.formVehiculo = this.formBuilder.group({
      marca: '',
      tipo: '',
      anio: ''
    })


  }

  ngOnInit(): void {
    if (!this.fromCrear) {

      this.formPersona.patchValue({
        cc: this.persona.cc,
        direccion: this.persona.direccion,
        fecha_nacimiento: this.persona.fecha_nacimiento,
        municipio: this.persona.municipio,
        nombres: this.persona.nombres,
        profesion: this.persona.profesion,
        sexo: this.persona.sexo,
        telefono: this.persona.telefono
      })

      this.formVehiculo.patchValue({
        marca: this.persona.vehiculo.marca,
        tipo: this.persona.vehiculo.tipo,
        anio: this.persona.vehiculo.anio
      })
      this.formPersona.controls['cc'].disable();

    }else{
      this.formPersona.controls['cc'].enable();

    }
  }


  emitGuardar() {
    this.btnGuardarEvent.emit({ "persona": this.formPersona.value, "vehiculo": this.formVehiculo.value })
  }

  emitEdit() {
    this.btnEditEvent.emit({ "persona": this.formPersona.value, "vehiculo": this.formVehiculo.value })

  }
}
