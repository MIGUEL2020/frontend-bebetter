import { AfterViewInit, OnInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { persona } from 'src/app/interfaces/persona';
import { Vehiculo } from 'src/app/interfaces/vehiculo';
import { PersonalService } from 'src/app/services/personal.service';



@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  personal: persona[] = [];
  displayedColumns: string[] = [
    'Persona',
    'Profesión',
    'Genero',
    'Telefono',
    'Fecha Nacimiento',
    'Residencia',
    ' '];
  dataSource = new MatTableDataSource<persona>(this.personal);
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  ccEliminar: string = "";
  vehiculoSelected: Vehiculo = {
    marca: '',
    tipo: '',
    anio: ''
  };
  selectedPerson: persona = {
    cc: '',
    direccion: '',
    fecha_nacimiento: new Date(),
    municipio: '',
    nombres: '',
    profesion: '',
    sexo: '',
    telefono: '',
    vehiculo: {
      marca: '',
      tipo: '',
      anio: ''
    }
  }


  constructor(private personalService: PersonalService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.getPersonal();
  }
  openCreateDialog(modalCrear: any) {
    const dialogRef = this.dialog.open(modalCrear);

  }

  openModalEdit(modalEdit: any, person: persona) {
    this.selectedPerson = person;
    const dialogRef = this.dialog.open(modalEdit);

  }

  getPersonal() {
    this.personalService.getPersonal().subscribe((resp: any) => {
      this.dataSource.data = resp as persona[];
      this.personal = resp as persona[];

    })
  }

  emicionGuardar(info: any) {
    let vehiculo: Vehiculo = info.vehiculo;

    let persona: persona = info.persona;
    persona.vehiculo = vehiculo;

    if (this.dataSource.data == null) {
      this.dataSource.data = []
    }

    this.dataSource.data.push(persona);

    this.personalService.addPersona(this.dataSource.data).subscribe((res: any) => {
      console.log(res);
      this.getPersonal();
    })
  }
  emicionEdit(infoEdited: any) {
    let vehiculo: Vehiculo = infoEdited.vehiculo;

    let persona: persona = infoEdited.persona;
    persona.vehiculo = vehiculo;

    if (this.dataSource.data == null) {
      this.dataSource.data = []
    }
    var pos = 0;
    for (let index = 0; index < this.dataSource.data.length; index++) {
      const element = this.dataSource.data[index];
      if (this.selectedPerson.cc == element.cc) {
        pos = index;
        break;
      }
    }

    this.personalService.editPerson(pos, persona).subscribe((resp: any) => {
      console.log(resp);
      this.getPersonal();

    })


  }

  openModalEliminar(modalEliminar: any, cc: string) {
    this.ccEliminar = cc;
    const dialogRef = this.dialog.open(modalEliminar);
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  openModalDetail(modalDetail: any, vehiculo: Vehiculo) {
    console.log(vehiculo);

    this.vehiculoSelected = vehiculo;
    const dialogRef = this.dialog.open(modalDetail);

  }
  eliminarPersona() {
    var pos = 0;

    for (let index = 0; index < this.dataSource.data.length; index++) {
      const element = this.dataSource.data[index];

      if (this.ccEliminar == element.cc) {
        pos = index;
        break;
      }
    }
    this.dataSource.data.splice(pos, 1);
    this.personalService.deletePerson(pos).subscribe((data: any) => {
      if (this.dataSource.data.length != 0) {
        this.personalService.addPersona(this.dataSource.data).subscribe((data: any) => {
          console.log(data);

          this.getPersonal();

        })
      }

    })


  }
}




/*   */